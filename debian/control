Source: libpoe-component-client-dns-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libnet-dns-perl <!nocheck>,
                     libpoe-perl <!nocheck>,
                     libtest-nowarnings-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     netbase <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libpoe-component-client-dns-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libpoe-component-client-dns-perl.git
Homepage: https://metacpan.org/release/POE-Component-Client-DNS
Rules-Requires-Root: no

Package: libpoe-component-client-dns-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libnet-dns-perl,
         libpoe-perl
Description: POE Component for performing asynchronous DNS queries
 POE::Component::Client::DNS is a wrapper for non-blocking DNS queries based on
 Net::DNS (libnet-dns-perl). It lets other tasks run while something is waiting
 for a nameserver response and allows multiple DNS servers to be queried at the
 same time. DNS client components are spawned as separate sessions rather than
 being created as proper objects; for full details, see the documentation.
